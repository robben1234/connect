<?php

/**
 * Created by PhpStorm.
 * User: robben1
 * Date: 02.11.15
 * Time: 21:48
 */
class output
{
    function __construct()
    {
        $this->tempDB = new database();
    }

    private function selectOutput() {
        try {
            $sql = 'SELECT joke.id, joketext, name, email FROM joke INNER JOIN author ON authorid = author.id';
            return $this->tempDB->pdo->query($sql);
        }
        catch(PDOException $e) {
            error::thisIsTheEnd($e);
        }
    }

    function go() {
        $result = $this->selectOutput();

        foreach ($result as $row) { //фетч не нужен, потому что PDOStatement в форич ведет себя как массив
            $jokes[] = array('id' => $row['id'],
                'text' => $row['joketext'],
                'name' => $row['name'],
                'email' => $row['email']);
        }

        include 'jokes.html.php';
    }
}