<?php

/**
 * Created by PhpStorm.
 * User: robben1
 * Date: 02.11.15
 * Time: 21:43
 */
class database
{
    function __construct()
    {
        try {
            $this->pdo = new PDO('mysql:host=localhost;dbname=ijdb', 'root', '1234');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->exec('SET NAMES "utf8"');
            //exec - возвращает количество строк затронутых запросов (когда результаты не нужны)
            //query - возвращает строки, затронутые запросом (когда результаты нужно вывести)
        } catch (PDOException $e) {
            error::thisIsTheEnd($e);
        }
    }

    function delete()
    {
        $postCopy = $_POST;
        if (ctype_digit($postCopy['id'])) {

            try {
                $sql = 'DELETE FROM joke WHERE id = :id';
                $s = $this->pdo->prepare($sql);
                $s->bindValue(':id', $postCopy['id']);
                $s->execute(); //execute - выполнение подготовленного скрипта, exec/query - неподготовленного
            } catch (PDOException $e) {
                error::thisIsTheEnd($e);
            }
            header('Location: .');
            exit();
        }

    }

    function  insert()
    {
        $postCopy = $_POST;

        try {
            $sql = 'INSERT INTO joke SET joketext = :joketext, jokedate = CURDATE(), authorid = 3';

            $s = $this->pdo->prepare($sql);
            $s->bindValue(':joketext', $postCopy['joketext']);
            $s->execute();
        } catch (PDOException $e) {
            error::thisIsTheEnd($e);
        }

        header('Location: .');
        exit();
    }
}