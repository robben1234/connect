<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/helpers.inc.php'; ?>
<html>
<head>
    <title>List of jokes</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="ajax.js"></script>
    <style>
        input {
            float: left;
            margin: 0 15px;
        }
    </style>
</head>
<body>
<header>
    <p><a href="?addjoke">Add your joke to db</a></p>
</header>
<main>
    <article>
        <h1>Here are all the jokes from db:</h1>
        <?php foreach ($jokes as $joke): ?>
            <form action="?deletejoke" method="post">
                <input type="hidden" name="id" value="<?php echo $joke['id']; ?>">
                <input type="submit" value="Delete">
            </form>

            <blockquote>
                <p>
                    <?php htmlout($joke['text']); ?>
                    (Автор <a href="mailto:
				<?php htmlout($joke['email']); ?>"><?php htmlout($joke['name']); ?></a>)
                </p>
            </blockquote>
        <?php endforeach; ?>
    </article>
    <article id="#content"></article>
</main>
</body>
</html>